%%%-------------------------------------------------------------------
%%% @author Jakub Szczepankiewicz
%%% @doc
%%% Generic server module for Game of Life that prints current board state.
%%% "Print and clear" shell UI
%%%
%%% @end
%%% Created : 25. gru 2015 12:51
%%%-------------------------------------------------------------------
-module(printsrv).
-behaviour(gen_server).
%% API
-export([change_cell/3,show_board/0]).
-export([start_link/2,init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================

start_link(Width, Height) ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [Width, Height], []).

change_cell(X,Y,NewState) ->
  gen_server:call(?SERVER, {change_state,X,Y,NewState}).

show_board() ->
  gen_server:call(?SERVER, {show}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================


%% Initializes the server
init([Width,Height]) ->
  {ok, create_board(Width,Height)}.


%% Handling call messages
handle_call({change_state, X, Y, Value}, From, Board) ->
  Reply = ok,
  NewBoard = change_state(X,Y,Value,Board),
  {reply, Reply, NewBoard};

handle_call({show}, From, Board) ->
  Reply = ok,
  print_board(Board),
  {reply, Reply, Board}.


%% Handling cast messages
handle_cast(_Msg, State) ->
  {noreply, State}.


%% Handling all non call/cast messages
handle_info(Info, State) ->
  io:fwrite("Info Msg ~w~n",[Info]),
  {noreply, State}.

%% Convert process state when code is changed
code_change(_OldVsn, State, _Extra) ->
  io:fwrite("Code change~n"),
  {ok, State}.

terminate(_Reason, _State) ->
  ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%% Generates empty (starting) board
create_board(Width, Height) ->
  [['_' || _ <- lists:seq(1,Width)] || _ <- lists:seq(1,Height)].

%% Updates state with X,Y coordinates on board
change_state(X, Y, NewState, Board) ->
  Row = lists:nth(Y,Board),
  NewRow = lists:sublist(Row,X-1) ++ [state_to_string(NewState)] ++ lists:nthtail(X,Row),
  lists:sublist(Board,Y-1) ++ [NewRow] ++ lists:nthtail(Y,Board).

%% Converts state atom to string representation
state_to_string(NewState) ->
  case NewState == alive of
    true -> '*';
    false -> '_'
  end.

%% Prints current board state to standard output
print_board(Board) ->
  io:format(os:cmd("clear")), %% clear terminal window (LINUX only)
  %io:format("New generation!~n"),
  lists:foreach(fun(Row) ->
    RowString = lists:concat(Row)++"~n",
    io:format(RowString)
                end
    ,Board).
