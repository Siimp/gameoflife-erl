%%%-------------------------------------------------------------------
%%% @author Jakub Szczepankiewicz
%%% @doc
%%% Main Game of Life module
%%%
%%% @end
%%% Created : 24. gru 2015 15:02
%%%-------------------------------------------------------------------
-module(life).

%% API
-export([start/3,timer/1,cell/6]).

%% Game of Life samples
-export([glider/0, lightweight_spaceship/0, rats/0]).


%%--------------------------------------------------------------------
%% @doc
%% Starts Game of Life module connected to simple generic server
%%
%% @spec start(Width, Height, Alive) -> {ServerPID}.
%% @end
%%--------------------------------------------------------------------
start(W,H,Alive) ->
  {ok, ServerPID} = printsrv:start_link(W,H),

  Board = create_board(W,H),
  init_board(W,H,Board),
  raiseDeadCells(W,Alive,Board),

  spawn(?MODULE,timer,[Board]),

  {ok, ServerPID}.

%%%===================================================================
%%% Game of Life samples (alive cells coordinates)
%%%===================================================================

glider() ->
  start(10,10,[{5,4},{3,5},{5,5},{4,6},{5,6}]).

lightweight_spaceship() ->
  start(15,15,[{6,6},{7,6},{4,7},{5,7},{7,7},{8,7},{4,8},
{5,8},{6,8},{7,8},{5,9},{6,9}]).

%%%===================================================================
%%% Internal functions
%%%===================================================================
%% Set cells to be alive on starting board
raiseDeadCells(Width, ToAlive, Board) ->
  lists:foreach(fun({X,Y}) -> lists:nth(Y*Width+X,Board) ! {set_state,alive} end, ToAlive).

%% Creates board
create_board(Width, Height) ->
  create_board_helper(Width,Height,0,[]).

create_board_helper(Width, Height, N, Board) when Width*Height == N -> Board;
create_board_helper(Width, Height, N, Board) ->
  [create_cell(N rem Width, N div Width) | create_board_helper(Width,Height,N+1,Board)].

%% Spawns cell process
create_cell(X, Y) ->
  spawn_link(?MODULE,cell,[X,Y,dead,[],0,0]).

%% Initialize all cells with neighbours
init_board(W, H, Board) ->
  init_board_helper(W,H,0,Board,Board).

init_board_helper(_,_,_,[],_) -> ok;
init_board_helper(Width,Height,N,[CellPID | Tail],Board) ->
  init_cell(CellPID,
    [lists:nth(1+utils:mod(N-Width-1, Width*Height),Board),
      lists:nth(1+utils:mod(N-Width, Width*Height),Board),
      lists:nth(1+utils:mod(N-Width+1, Width*Height),Board),
      lists:nth(1+utils:mod(N-1, Width*Height),Board),
      lists:nth(1+utils:mod(N+1, Width*Height),Board),
      lists:nth(1+utils:mod(N+Width-1, Width*Height),Board),
      lists:nth(1+utils:mod(N+Width, Width*Height),Board),
      lists:nth(1+utils:mod(N+Width+1, Width*Height),Board)]),
  init_board_helper(Width,Height,N+1,Tail,Board).

%% Initialize neighbours for cell
init_cell(CellPID, Neighbours) ->
  CellPID ! {init,Neighbours}.

%% Main cell process
cell(X,Y,State,Neighbours, NLiving, NReceived) ->
  receive
    {init, NewNeighbours} ->
      cell(X,Y,State,NewNeighbours,NLiving,NReceived);
    {set_state, NewState} ->
      cell(X,Y,NewState,Neighbours,NLiving,NReceived);
    {tic} ->
      send_state(State,Neighbours),
      cell(X,Y,State,Neighbours,NLiving,NReceived);
    {state,alive} ->
      case NReceived of
        7 -> cell(X,Y,update_cell(X,Y,State,NLiving+1),Neighbours,0,0);
        _ -> cell(X,Y,State,Neighbours,NLiving+1,NReceived+1)
      end;
    {state,dead} ->
      case NReceived of
        7 -> cell(X,Y,update_cell(X,Y,State,NLiving),Neighbours,0,0);
        _ -> cell(X,Y,State,Neighbours,NLiving,NReceived+1)
      end
  end.

%% Sends current cell state to neighbours
%send_state(State, Neighbours) ->
%  lists:foreach(fun(Neighbour) -> Neighbour ! {state,State} end, Neighbours).
send_state(_,[]) -> ok;
send_state(State,[H|T]) -> H ! {state,State}, send_state(State,T).

%% Updates cell state based on number of living neighbours
%% Sends update info to gen_server
update_cell(X, Y, State, NLiving) ->
  if
    NLiving < 2 ->
      printsrv:change_cell(X+1,Y+1,dead),
      dead;
    NLiving == 2 ->
      printsrv:change_cell(X+1,Y+1,State),
      State;
    NLiving == 3 ->
      printsrv:change_cell(X+1,Y+1,alive),
      alive;
    NLiving >= 4 ->
      printsrv:change_cell(X+1,Y+1,dead),
      dead
  end.

%% Process that signals all cells every interval
%% Sends print board request to gen_server
timer(Board) ->
  receive
    after 1000 ->
      tic(Board)
  end,
  printsrv:show_board(),
  timer(Board).

%% Sends clock tick to all cells
tic([])-> ok;
tic([H|T]) -> H ! {tic}, tic(T).
