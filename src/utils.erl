%%%-------------------------------------------------------------------
%%% @author Jakub Szczepankiewicz
%%% @doc
%%% Game of Life module utilities
%%%
%%% @end
%%% Created : 24. gru 2015 16:51
%%%-------------------------------------------------------------------
-module(utils).
-export([mod/2]).

%% Modulo that always returns a non-negative value
mod(X,Y) when X > 0 -> X rem Y;
mod(X,Y) when X < 0 -> Y + X rem Y;
mod(0,_) -> 0.
